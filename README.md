# Pypftp

Pypftp is a Python script that provides a more convenient command line
interface for access to the HPSS tape archive. The Python ftplib
package is used for most operations like ls, mkdir, size etc. The HPSS
pftp client is only used for file up- and download.

## Prerequisites

### Python version

The program should work with any recent (2.7 and newer) Python
version.

On mistral, pypftp is tested with `python/3.5.2`, `python/2.7.12` and
`anaconda3/bleeding_edge` (recommended). Check your current Python
version with the `module list` command.  Use `module load` to load one
of the above python versions.

### HPSS pftp client

By default, the pftp module is pre-loaded into the environment on
Mistral.  Please, verify, using the `module list` command, that you did
not inadvertently unload it.

### Authentication

For passwordless access to HPSS, the current implementation relies on
credentials stored in your `~/.netrc` file. This might change in the
future when an alternative authentication mechanism (GSSAPI) is made
available.

## Getting started

Add the path to the pypftp script to the search path for programs,
e.g.:

    Bourne shell and derivatives:
    $ PATH=/path/to/pypftp:${PATH}
    csh and derivates:
    $ setenv PATH /path/to/pypftp:${PATH}

Use the `--help` option to get an overview of the tool commands and
options:

    $ pypftp --help

    Usage: pypftp [OPTIONS] COMMAND [ARGS]...

      A command line interface for HPSS (tape archive).

    Options:
      --help  Show this message and exit.

    Commands:
      download  Download files from tape
      exists    check if PATH exists on tape.
      isdir     checks if PATH is a directory.
      isfile    checks if PATH is a file.
      islink    checks if PATH is a link.
      ls        List directory contents on tape.
      mkdir     creates PATH on tape.
      size      Prints file size in bytes.
      upload    Upload files to tape


To get information on a command, use `pypftp <command> --help`, e.g. for
the `ls` command use:

    $ pypftp ls --help
    Usage: pypftp ls [OPTIONS] PATH

      List directory contents on tape.

      PATH: path on tape

    Options:
      -l      long listing
      -r      reverse listing
      -t      sort by mtime
      -h      human readable size
      --help  Show this message and exit.
